# wgcna_to_cytoscape.R

This script provides a function to export a WGCNA network to tabular node and edge files for use with cytoscape. To use it, first generate a network object using `WGCNA::blockwiseModules(saveTOMs = TRUE)` and then pass it to the `wgcna_to_cytoscape()` function. For example:

```r
library(WGCNA)
library(devtools)
source_url("https://gitlab.com/salk-tm/wgcna-to-cytoscape/-/raw/main/wgcna_to_cytoscape.R")
...
net = blockwiseModules(datExpr, maxBlockSize = 5000, saveTOMs = TRUE)
wgcna_to_cytoscape(net)
```

It is important to note that if `maxBlockSize` is less than the total number of genes in `datExpr`, only the genes of the first block will be included in the cytoscape output. For full results, set `maxBlockSize` equal to or greater than the total number of genes.

By default, output files will be written in a directory called `output_for_cytoscape`. To set a different output dir, use the `output_dir` parameter:
```r
wgcna_to_cytoscape(net, output_dir = "alt_output_dir")
```

If the `saveTOMFileBase` parameter of `blockwiseModules()` is set, use the `tom_file_base` parameter of `wgcna_to_cytoscape()` to match it.
```r
net = blockwiseModules(datExpr, maxBlockSize = 5000, saveTOMs = TRUE,
                       saveTOMFileBase = "base")
wgcna_to_cytoscape(net, tom_file_base = "base")
```
